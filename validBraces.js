'use strict';

/**
 * Проверяет корректность открытия/закрытия скобок во входной строке,
 * все остальные символы игнорируются, пустая строка считается валидной.
 *
 * @param {string} inputStr
 * @returns {boolean}
 */
function validBraces (inputStr) {
    var openBraces = ['(', '{', '['];
    var closeBraces = [')', '}', ']'];
    var stack = [];

    if (typeof inputStr !== 'string') {
        throw new TypeError('validBraces expects string argument, but got ' +
            Object.prototype.toString.call(inputStr));
    }

    for (var i = 0; i < inputStr.length; i++) {
        var braceIdx = openBraces.indexOf(inputStr[i]);

        if (braceIdx !== -1) {
            stack.push(braceIdx);
        } else if ((braceIdx = closeBraces.indexOf(inputStr[i])) !== -1) {
            var lastOpenBrace = stack.pop();

            if (lastOpenBrace === undefined || lastOpenBrace !== braceIdx) {
                return false;
            }
        }
    }

    return stack.length === 0;
}

// Тесты) Должны быть в отдельном файле и с использованием фреймворка
console.log('validBraces tests');

try {
    validBraces(12334343);
    console.log('Invalid argument type. passed =', false);
} catch (e) {
    console.log('Invalid argument type. passed =', true);
}

console.log('Test () must be true. passed =', validBraces('()') === true);

console.log('Test [) must be false. passed =', validBraces('[)') === false);

console.log('Test {}[]() must be true. passed =', validBraces('{}[]()') === true);

console.log('Test ([{}]) must be true. passed =', validBraces('([{}])') === true);

console.log('Test ())({}}{()][][ must be false. passed =',
    validBraces('())({}}{()][][') === false);

console.log('Test empty string must be true. passed =', validBraces('') === true);

console.log('Test (other{chars}). passed =', validBraces('(other{chars})') === true);
