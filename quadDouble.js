'use strict';

/**
 * Возвращает true в случае нахождения 4-х, идущих подряд цифр в первом числе
 * и двух, таких же, идущих подряд, во втором
 * @param {number} num1
 * @param {number} num2
 * @returns {boolean}
 */
function quadDouble (num1, num2) {
    var str = num1 + '|' + num2;
    return str.match(/(\d)\1{3}.*\|.*\1{2}/g) !== null;
}

// Тесты) Должны быть в отдельном файле и с использованием фреймворка

console.log('quadDouble tests');

console.log('Test 45568411115, 11223344 must be true. passed = ',
    quadDouble(45568411115, 11223344) === true);

console.log('Test 11112344445, 442253 must be true. passed = ',
    quadDouble(11112344445, 442253) === true);

console.log('Test 12222345, 123452 must be false. passed = ',
    quadDouble(12222345, 123452) === false);

console.log('Test 12345, 12345 must be false. passed = ',
    quadDouble(12345, 12345) === false);

console.log('Test 33338411115, 33223344 must be true. passed = ',
    quadDouble(33338411115, 33223344) === true);

console.log('Test 44333384, 2233 must be true. passed = ',
    quadDouble(44333384, 2233) === true);

