'use strict';

/**
 * Группирует элементы массива по ключу создаваемому функцией keyFunc
 *
 * @param {function} keyFunc - group key maker function
 * @return {object} - object with grouped elements
 */
Array.prototype.groupBy = Array.prototype.groupBy || function (keyFunc) {
    if (!Array.isArray(this)) {
        throw new Error('Array.groupBy can be used only in array context');
    }

    if (keyFunc && typeof keyFunc !== 'function') {
        throw new TypeError('Argument keyFunc in Array.groupBy must be function, but got' +
            Object.prototype.toString.call(keyFunc)
        );
    }

    return this.reduce(function (grouped, item) {
        var key = keyFunc ? keyFunc(item) : item;

        if (key in grouped) {
            grouped[key].push(item);
        } else {
            grouped[key] = [item];
        }
        return grouped;
    }, {});
};

// Тесты) Должны быть в отдельном файле и с использованием фреймворка

/**
 * Очень спорная реализация deepEqual,
 * используется по причине отсутствия фреймворка для тестирования
 *
 * @param {*} obj1
 * @param {*} obj2
 */
var isEqual = function (obj1, obj2) {
    return JSON.stringify(obj1) === JSON.stringify(obj2);
};

var inputArray, expectedObject, keyFunc, resultObject;

console.log('Array.groupBy tests');

// Test empty array
inputArray = [];
expectedObject = {};
resultObject = inputArray.groupBy();
console.log('groupBy with epmty array passed =', isEqual(expectedObject, resultObject));

// Test without keyFunc. key = element
inputArray = [1, 2, 3, 2, 4, 1, 5, 1, 6];
expectedObject = {
    1: [1, 1, 1],
    2: [2, 2],
    3: [3],
    4: [4],
    5: [5],
    6: [6]
};
resultObject = inputArray.groupBy();

console.log('groupBy without keyFunc passed =', isEqual(expectedObject, resultObject));

// Test with keyFunc
inputArray = [1, 2, 3, 2, 4, 1, 5, 1, 6];
expectedObject = {
    0: [3, 6],
    1: [1, 4, 1, 1],
    2: [2, 2, 5]
};
keyFunc = function (val) { return val % 3; };
resultObject = inputArray.groupBy(keyFunc);

console.log('groupBy with keyFunc passed =', isEqual(expectedObject, resultObject));

// Test any types without keyFunc. key = element
inputArray = [1, 'a', 'b', 1, null, 'a', undefined, undefined, { a: 1 }, { b: 2 }];
expectedObject = {
    1: [1, 1],
    a: ['a', 'a'],
    b: ['b'],
    null: [null],
    undefined: [null, null], // undefined stringify as null
    '[object Object]': [ { 'a': 1 }, { 'b': 2 } ]
};
resultObject = inputArray.groupBy();
console.log('groupBy with any types without keyFunc passed = ', isEqual(expectedObject, resultObject));

// Test with non-array context
inputArray = {
    1: 1,
    2: 1,
    3: 2,
    4: 1
};
try {
    [].groupBy.call(inputArray);
    console.log('groupBy with non array context passed =', false);
} catch (e) {
    console.log('groupBy with non array context passed =', true);
}

// Test with non function argument
keyFunc = 'invalid';
try {
    [].groupBy(keyFunc);
    console.log('groupBy with non function argument passed =', false);
} catch (e) {
    console.log('groupBy with non function argument passed =', true);
}

